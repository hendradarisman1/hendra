<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class gapura extends CI_Controller {
	
	public function index()
	{
		$session =$this->session->userdata('uName'); 
            // var_dump($this->session->userdata);
        if($session == true){
            redirect('/dashboard/');
        }

		$data['style']= $this->load->view('include/styles', NULL, TRUE);
		$data['scripts']= $this->load->view('include/scripts', NULL, TRUE);
		$this->load->view('index', $data);

	}

	public function otorisasi(){
		$this->load->model('User');
		$nopeg = $_POST['nopeg'];
		$display = $this->User->setOtorisasi($nopeg);
		var_dump($display);
	}

	public function loginProc(){
		$user = $_POST['username'];
		$pass = $_POST['password'];
		$this->load->model('User');
		$hasil = $this->User->login($user, $pass);
		if ($hasil == 'frozed') {
			$this->session->set_flashdata('response',"<div class='alert alert-warning'><b>Your Account is been frozed!</b></div>");
			redirect('/gapura/');
		}
		if(isset($hasil["uName"])){
			$this->session->set_userdata($hasil);
			redirect('/dashboard/');
		}
		else{
			$this->session->set_flashdata('response',"<div class='alert alert-danger'><b>Invalid Username or Password!</b></div>");
			redirect('/gapura/');
		}
		// echo $hasil;
	}
	public function profile(){
		$this->load->view('biodata');
	}
}
?>