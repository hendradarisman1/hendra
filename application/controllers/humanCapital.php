<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class humanCapital extends CI_Controller {
	
	// public function index(){
 //            $data['style']= $this->load->view('include/styles', NULL, TRUE);
 //            $data['scripts']= $this->load->view('include/scripts', NULL, TRUE);
 //            $data['nopeg'] = $this->session->userdata('uName');
 //            $this->load->view('dashboard/payslip', $data);
 //        }

	public function dashboard(){
		redirect('/dashboard/');
	}

	public function cvpdf(){
		$this->load->model('User');
		$session =$this->session->userdata('uName'); 
            if($session == false){
                redirect(base_url());
            }
        //check status non aktif dan hapus session user!
		$status = $this->User->cekStatus($session);
		if($status == 'NON AKTIF'){
			$this->session->unset_userdata('uName');
			$this->session->set_flashdata('response',"<div class='alert alert-warning'><b>Your Account is been frozed!</b></div>");
			redirect('gapura/');
		}
		// cek foto udah ada atau belom, kalo gak ada, simpen dulu
		if(!file_exists(FCPATH."upload/img/image".$this->session->userdata('uName').".jpg")){
			// Simpen foto dulu
			$photo = $this->User->getPhoto($this->session->userdata('uName'));
			if($photo == 1){
                define('DIRECTORY', './upload/img/');
				define('source', 'assets/img/');
				copy(source.'admin.png', DIRECTORY.'/image'.$this->session->userdata('uName').'.jpg');
            }
            else{
                define('DIRECTORY', './upload/img/');
				$content = file_get_contents($photo);
				file_put_contents(DIRECTORY . '/image'.$this->session->userdata('uName').'.jpg', $content);
            }
		}

		// $data['foto'] = base_url()."upload/img/image.jpg";
		$this->load->model('CvModel');
		$hasil = $this->CvModel->isiCV($this->session->userdata('uName'));
		$data['award'] = $_POST['award'];
		$data['punishment'] = $_POST['punishment'];
		$data['special'] = $_POST['special'];
		$data['uname'] = $session;
		$data['cv'] = $hasil;
    	$this->load->model("Administrative_Model");
        $data['users'] = $this->Administrative_Model->getAllUser();
    	$this->load->view('dashboard/cvgapura', $data);
	}

	// Testing purposes
	/*public function cvPage(){
		$this->load->model('User');
		$session =$this->session->userdata('uName'); 
            if($session == false){
                redirect(base_url());
            }
        //check status non aktif dan hapus session user!
		$status = $this->User->cekStatus($session);
		if($status == 'NON AKTIF'){
			$this->session->unset_userdata('uName');
			$this->session->set_flashdata('response',"<div class='alert alert-warning'><b>Your Account is been frozed!</b></div>");
			redirect('gapura/');
		}

		$this->load->model('CvModel');
		$hasil = $this->CvModel->isiCV($this->session->userdata('uName'));
		$data['cv'] = $hasil;
		$this->load->view('signup',$data);
	}*/

	public function payslip(){
		$this->load->model('User');
		$session =$this->session->userdata('uName'); 
            if($session == false){
                redirect(base_url());
            }
        //check status non aktif dan hapus session user!
		$status = $this->User->cekStatus($session);
		if($status == 'NON AKTIF'){
			$this->session->unset_userdata('uName');
			$this->session->set_flashdata('response',"<div class='alert alert-warning'><b>Your Account is been frozed!</b></div>");
			redirect('gapura/');
		}

		$this->load->model('Payslip');
		// variabel yang akan dipassing
		$nopeg = $this->session->userdata('uName');
		$seqNumber;
		// cek apakah pegawai aktif
		$aktif=$this->Payslip->cekAktif($nopeg);
		if($aktif == 1){
			$seqNumber = $this->Payslip->ambilSeqNumber($nopeg);
			// var_dump($seqNumber);
			$hasil = $this->Payslip->loadPayslip($nopeg,$seqNumber[1]);
		}
		else{
			$seqNunmber = 0;
		}
        $photo = $this->User->getPhoto($session);
		$data['foto'] = $photo;
		$data['seqnumber'] = $seqNumber;
        $data['slip'] = $hasil;
        $data['nopeg'] = $nopeg;
        $otorisasi = $this->User->setOtorisasi($this->session->userdata('uName'));
        $data['otorisasi'] = $otorisasi;
        $data['aktif'] = $aktif;
        $this->load->view('dashboard/payslip', $data);
	}
	public function showPayslip(){
		$this->load->model('User');
		$session =$this->session->userdata('uName');
		$status = $this->User->cekStatus($session);
		/*if($status == 'NON AKTIF'){
			$this->session->unset_userdata('uName');
			$this->session->set_flashdata('response',"<div class='alert alert-warning'><b>Your Account is been frozed!</b></div>");
			redirect('gapura/');
		}*/
        $nopeg = $this->session->userdata('uName');
        $this->load->model('Payslip');
		if (isset($_POST['payslipoption'])) {
			$checkpayslip = $_POST['payslipoption'];
			// variabel yang akan dipassing
			
			//$seqNumber;
			$aktif=$this->Payslip->cekAktif($nopeg);
			if($aktif == 1){
				$hasil = $this->Payslip->loadPayslip($nopeg,$checkpayslip);
			}
			else{
				$seqNumber = 0;
			}
		}elseif(!isset($_POST['payslipoption'])){
			$seqNumber = $this->Payslip->ambilSeqNumber($nopeg);
			// variabel yang akan dipassing
			
			//$seqNumber;
			$aktif=$this->Payslip->cekAktif($nopeg);
			if($aktif == 1){
				$hasil = $this->Payslip->loadPayslip($nopeg,$seqNumber[0][1]);
			}
			else{
				$seqNumber = 0;
			}
		}
		$data['status'] = $status;
		$data['slip'] = $hasil;
		$data['seqnumber'] = $seqNumber;
		$data['aktif'] = $aktif;
		$data['checkpayslip'] = $checkpayslip;
        $this->load->view('dashboard/showPayslip', $data);
	}
	public function deductions(){
		$this->load->model('User');
		$session =$this->session->userdata('uName'); 
            if($session == false){
                redirect(base_url());
            }
        //check status non aktif dan hapus session user!
		$status = $this->User->cekStatus($session);
		if($status == 'NON AKTIF'){
			$this->session->unset_userdata('uName');
			$this->session->set_flashdata('response',"<div class='alert alert-warning'><b>Your Account is been frozed!</b></div>");
			redirect('gapura/');
		}
        $photo = $this->User->getPhoto($session);
		$data['foto'] = $photo;
        $otorisasi = $this->User->setOtorisasi($this->session->userdata('uName'));
		$data['otorisasi'] = $otorisasi;
		$this->load->view('dashboard/deductions',$data);
	}

	public function deductionsProc(){
		$session = $this->session->userdata('uName');
            if($session == false){
                redirect(base_url());
            }
		$this->load->model('Upload');
		$this->load->model('User');
		//check status non aktif dan hapus session user!
		$status = $this->User->cekStatus($session);
		if($status == 'NON AKTIF'){
			$this->session->unset_userdata('uName');
			$this->session->set_flashdata('response',"<div class='alert alert-warning'><b>Your Account is been frozed!</b></div>");
			redirect('gapura/');
		}
		$nopeg = $_POST['nopeg'];
		$wageType = $_POST['wageType'];
		$amount = $_POST['amount'];
		$start = $_POST['start'];
		$startDate = substr($start,0,4).substr($start,5,2).substr($start,8,2);
		$end = $_POST['end'];
		$endDate = substr($end,0,4).substr($end,5,2).substr($end,8,2);

		$nopegcek = $this->User->cekNoPegExists($nopeg);

		if ($nopegcek == 0 ) {
			$this->session->set_flashdata('response',"<div class='alert alert-warning'><b>Nomor Pegawai $nopeg tidak ada dalam SAP System!</b></div>");
			redirect('humanCapital/deductions/');
		}
		if ($amount < 100) {
			$this->session->set_flashdata('response',"<div class='alert alert-warning'><b>Amount yang di masukan tidak boleh lebih kecil dari 100!</b></div>");
			redirect('humanCapital/deductions/');
		}

		$this->Upload->infoType14($nopeg ,$wageType ,$startDate ,$endDate, $amount);
		$this->session->set_flashdata('response',"<div class='alert alert-success'><b>Upload infotype 14 berhasil dilakukan pada Nomor Pegawai $nopeg</b></div>");
		redirect('humanCapital/deductions/');
	}

	public function multipleDeductionsProc(){
		$config = array(
			'upload_path' => "./upload/txt",
			'allowed_types' => "txt",
			'overwrite' => TRUE,
			'max_size' => "2048000" // Can be set to particular file size , here it is 2 MB(2048 Kb)

			);
			$this->load->library('upload', $config);
			if($this->upload->do_upload())
			{
			$data = array('upload_data' => $this->upload->data());
			$file_name = $data['upload_data']['file_name']; //nama filenya
			}
			else
			{
				echo "macet";
				$error = array('error' => $this->upload->display_errors());
			} //returns array yg menyimpan seluruh data dari file
	}

	public function additionalPayments(){
		$this->load->model('User');
		$session =$this->session->userdata('uName'); 
            if($session == false){
                redirect(base_url());
            }
        //check status non aktif dan hapus session user!
		$status = $this->User->cekStatus($session);
		if($status == 'NON AKTIF'){
			$this->session->unset_userdata('uName');
			$this->session->set_flashdata('response',"<div class='alert alert-warning'><b>Your Account is been frozed!</b></div>");
			redirect('gapura/');
		}
        $photo = $this->User->getPhoto($session);
		$data['foto'] = $photo;
        $otorisasi = $this->User->setOtorisasi($this->session->userdata('uName'));
		$data['otorisasi'] = $otorisasi;
		$this->load->view('dashboard/additionalPayment',$data);
	}

	public function additionalPaymentProc(){
		$session = $this->session->userdata('uName');
            if($session == false){
                redirect(base_url());
            }
		$this->load->model('Upload');
		$this->load->model('User');
		//check status non aktif dan hapus session user!
		$status = $this->User->cekStatus($session);
		if($status == 'NON AKTIF'){
			$this->session->unset_userdata('uName');
			$this->session->set_flashdata('response',"<div class='alert alert-warning'><b>Your Account is been frozed!</b></div>");
			redirect('gapura/');
		}
		$nopeg = $_POST['nopeg'];
		$wageType = $_POST['wageType'];
		$amount = $_POST['amount'];
		$start = $_POST['start'];
		$startDate = substr($start,0,4).substr($start,5,2).substr($start,8,2);
		$end = $_POST['end'];
		$endDate = substr($end,0,4).substr($end,5,2).substr($end,8,2);

		$nopegcek = $this->User->cekNoPegExists($nopeg);


		if ($nopegcek == 0 ) {
			$this->session->set_flashdata('response',"<div class='alert alert-warning'><b>Nomor Pegawai $nopeg tidak ada dalam SAP System!</b></div>");
			redirect('humanCapital/additionalPayments/');
		}
		if ($amount < 100) {
			$this->session->set_flashdata('response',"<div class='alert alert-warning'><b>Amount yang di masukan tidak boleh lebih kecil dari 100!</b></div>");
			redirect('humanCapital/additionalPayments/');
		}

		$this->Upload->infoType15($nopeg ,$wageType ,$startDate ,$endDate, $amount);
		$this->session->set_flashdata('response',"<div class='alert alert-success'><b>Upload infotype 15 berhasil dilakukan pada Nomor Pegawai $nopeg</b></div>");
		redirect('humanCapital/additionalPayments/');
	}

	public function multipleAdditionalPaymentsProc(){
		$this->load->model('User');
		$this->load->model('Upload');
		$config = array(
			'upload_path' => "./upload/txt",
			'allowed_types' => "txt",
			'overwrite' => TRUE,
			'max_size' => "2048000" // Can be set to particular file size , here it is 2 MB(2048 Kb)

			);
			$this->load->library('upload', $config);
			if($this->upload->do_upload())
			{
			$data = array('upload_data' => $this->upload->data());
			$file_path = $data['upload_data']['full_path']; //nama filenya
			}
			else
			{
				echo "macet";
				$error = array('error' => $this->upload->display_errors());
			} //returns array yg menyimpan seluruh data dari file
			// echo $file_path;

			$this->Upload->multipleInfoType15($file_path);
	}

	public function familyMembers(){
		$this->load->model('User');
		$session =$this->session->userdata('uName'); 
            if($session == false){
                redirect(base_url());
            }
        //check status non aktif dan hapus session user!
		$status = $this->User->cekStatus($session);
		if($status == 'NON AKTIF'){
			$this->session->unset_userdata('uName');
			$this->session->set_flashdata('response',"<div class='alert alert-warning'><b>Your Account is been frozed!</b></div>");
			redirect('gapura/');
		}
        $photo = $this->User->getPhoto($session);
		$data['foto'] = $photo;
        $otorisasi = $this->User->setOtorisasi($this->session->userdata('uName'));
		$data['otorisasi'] = $otorisasi;
		$this->load->view('dashboard/familyMember', $data);
	}

	public function familyMembersProc(){
		$session = $this->session->userdata('uName');
            if($session == false){
                redirect(base_url());
            }
		$this->load->model('Upload');
		$this->load->model('User');
		//check status non aktif dan hapus session user!
		$status = $this->User->cekStatus($session);
		if($status == 'NON AKTIF'){
			$this->session->unset_userdata('uName');
			$this->session->set_flashdata('response',"<div class='alert alert-warning'><b>Your Account is been frozed!</b></div>");
			redirect('gapura/');
		}
		$nopeg = $_POST['nopeg'];
		$nama = $_POST['nama'];
		$hubungan = $_POST['hubungan'];
		$gender = $_POST['gender'];
		$konsesi = $_POST['konsesi'];
		if($konsesi == ''){
			$konsesi = "01";
		}
		$birth = $_POST['birth'];
		$birthdate = substr($birth,0,4).substr($birth,5,2).substr($birth,8,2);
		$birthplace = $_POST['birthplace'];

		$nopegcek = $this->User->cekNoPegExists($nopeg);
		
		if ($nopegcek == 0 ) {
			$this->session->set_flashdata('response',"<div class='alert alert-warning'><b>Nomor Pegawai $nopeg tidak ada dalam SAP System!</b></div>");
			redirect('humanCapital/familyMembers/');
		}

		$this->Upload->infoType21($nopeg, $hubungan, $birthdate, $birthplace, $gender, $nama, $konsesi);
		$this->session->set_flashdata('response',"<div class='alert alert-success'><b>Upload infotype 21 berhasil dilakukan pada Nomor Pegawai $nopeg</b></div>");
		redirect('humanCapital/familyMembers/');
	}

	public function education(){
		$this->load->model('User');
		$session =$this->session->userdata('uName'); 
            if($session == false){
                redirect(base_url());
            }
        //check status non aktif dan hapus session user!
		$status = $this->User->cekStatus($session);
		if($status == 'NON AKTIF'){
			$this->session->unset_userdata('uName');
			$this->session->set_flashdata('response',"<div class='alert alert-warning'><b>Your Account is been frozed!</b></div>");
			redirect('gapura/');
		}
        $photo = $this->User->getPhoto($session);
        $data['foto'] = $photo;
        $otorisasi = $this->User->setOtorisasi($this->session->userdata('uName'));
		$data['otorisasi'] = $otorisasi;
		$this->load->view('dashboard/education', $data);
	}

	public function educationProc(){
		$session = $this->session->userdata('uName');
            if($session == false){
                redirect(base_url());
            }
		$this->load->model('Upload');
		$this->load->model('User');
		//check status non aktif dan hapus session user!
		$status = $this->User->cekStatus($session);
		if($status == 'NON AKTIF'){
			$this->session->unset_userdata('uName');
			$this->session->set_flashdata('response',"<div class='alert alert-warning'><b>Your Account is been frozed!</b></div>");
			redirect('gapura/');
		}
		$nopeg = $_POST['nopeg'];
		$educationType = $_POST['educationType'];
		$start = $_POST['start'];
		$startdate = substr($start,0,4).substr($start,5,2).substr($start,8,2);
		$end = $_POST['end'];
		$endDate = substr($end,0,4).substr($end,5,2).substr($end,8,2);
		$lokasi = $_POST['lokasi'];
		$country = $_POST['country'];
		$courseName = $_POST['courseName'];
		//var_dump($nopeg,$educationType,$startdate,$end,$lokasi,$country,$courseName);
		$nopegcek = $this->User->cekNoPegExists($nopeg);
		
		if ($nopegcek == 0 ) {
			$this->session->set_flashdata('response',"<div class='alert alert-warning'><b>Nomor Pegawai $nopeg tidak ada dalam SAP System!</b></div>");
			redirect('humanCapital/education/');
		}

		$this->Upload->infoType22($nopeg,$educationType,$startdate,$endDate,$lokasi,$country,$courseName);
		$this->session->set_flashdata('response',"<div class='alert alert-success'><b>Upload infotype 22 berhasil dilakukan pada Nomor Pegawai $nopeg</b></div>");
		redirect('humanCapital/education/');
	}	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */