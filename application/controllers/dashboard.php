<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class dashboard extends CI_Controller {
    
        public function index(){
            $session =$this->session->userdata('uName'); 
            // var_dump($this->session->userdata);
            if($session == false){
                redirect(base_url());
            }
            $this->load->model('User');
            //check status non aktif dan hapus session user!
            $status = $this->User->cekStatus($session);
            if($status == 'NON AKTIF'){
                $this->session->unset_userdata('uName');
                $this->session->set_flashdata('response',"<div class='alert alert-warning'><b>Your Account is been frozed!</b></div>");
                redirect('gapura/');
            }
            $otorisasi = $this->User->setOtorisasi($this->session->userdata('uName'));
            // $data['style']= $this->load->view('include/styles', NULL, TRUE);
            // $data['scripts']= $this->load->view('include/scripts', NULL, TRUE);
            $data['nopeg'] = $this->session->userdata('uName');
            $data['otorisasi'] = $otorisasi;
            $this->load->model('User');
            $photo = $this->User->getPhoto($session);
            // define('DIRECTORY', './upload/img');
            // $content = file_get_contents($photo);
            // file_put_contents(DIRECTORY . '/image.jpg', $content);
            if($photo == 1){
                $data['foto'] = base_url().'assets/img/admin.png';
            }
            else{
                $data['foto'] = $photo;
            }
            
            $this->load->view('dashboard/index', $data);
        }
        public function admin(){
            redirect('/administrative/');
        }
        public function logout(){
            $this->load->model('User');
            $this->User->logout();
            redirect(base_url());
        }
    }
?>