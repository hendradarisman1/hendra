<?php
if (!defined('BASEPATH')) exit('No direct script access allowed'); 

class Pdf_helper {
  public function tcpdf()
  {
    require_once APPPATH."tcpdf/config/lang/eng.php";
    require_once APPPATH."tcpdf/tcpdf.php";
  }
}
?>
