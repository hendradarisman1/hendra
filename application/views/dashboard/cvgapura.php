<?php
//tcpdf();
//require_once APPPATH."libraries/tcpdf/config/lang/eng.php";
require_once APPPATH."libraries/tcpdf/tcpdf.php";
$obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$obj_pdf->SetCreator(PDF_CREATOR);
$title = "Curriculum Vitae";
$obj_pdf->SetTitle($title);
$obj_pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH);
$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$obj_pdf->SetDefaultMonospacedFont('helvetica');
$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$obj_pdf->SetFont('helvetica', '', 9);
$obj_pdf->setFontSubsetting(false);
$obj_pdf->AddPage();
//HTML + PHP TO PDF CODE HERE!
//BASIC PAGE
$html = '
    <style>
        div{
                    background-color: yellow;
        }
        img{
          height: 120px;
          width: 170px;
          text-align: left;
          vertical-align: text-top;
        }
        h2{
          text-align: left;
          color : #525f30;
          text-transform: uppercase;
        }
        th{
          text-transform: uppercase;
          background-color : #8fca6c;
          color : black;
        }
        strong{
          text-transform: uppercase;
        }
        ul{
          font-weight: bold;
        }
        span{
           vertical-align: text-top;
        }
        b{
          color : #525f30;
        }
        td{
          color : grey;
        }
        h4{
          color : grey;
        }
    </style>
    <table>
    <tr>
    ';
$html .='<td><img src="upload/img/image'.$uname.'.jpg" alt="logo-xs"></td>';
$html .='<td><h2>BIODATA</h2>';
$html .='<h3><strong style="letter-spacing: 5px;">'.$cv['dataPribadi']['nama'].'</strong></h3>';
$html .='<strong><b>NOPEG</b> : '.$cv['dataPribadi']['nopeg'].'</strong><br>';
$html .='<strong><b>JABATAN</b> : '.$cv['dataPribadi']['jabatan'].'</strong><br>';
$html .='<strong><b>UNIT KERJA</b> : '.$cv['dataPribadi']['divisi'].'</strong><br>';
$html .='<strong><b>LOKASI</b> : '.$cv['dataPribadi']['lokasi'].'</strong>';
$html .='</td></tr></table>';

//    <span><img src="assets/img/logo_gapura.png" alt="logo-xs">BIODATA<br><span>ayam</span></span>
//PROFILE RENDERING
$html .='
    <h2>Profil</h2>
    <hr> 
';
$html .= "<h4><b>Tempat/Tgl Lahir</b> : <mark>".$cv['dataPribadi']['ttl']."</mark></h4>";
$html .= "<h4><b>Jenis Kelamin</b> : <strong>".$cv['dataPribadi']['gender']."</strong></h4>";
$html .= "<h4><b>Agama</b> : <strong>".$cv['dataPribadi']['agama']."</h4>";
$html .= "<h4><b>Tgl Mulai Bekerja</b> : <strong>".$cv['dataPribadi']['hireDate']."</strong></h4>";
$html .= "<h4><b>Status Karyawan</b> : <strong>".$cv['dataPribadi']['statusPeg']."</strong></h4>";
$html .= "<h4><b>Status Keluarga</b> : <strong>".$cv['dataPribadi']['statusNikah']."</strong></h4>";
$html .= "<h4><b>Pendidikan Terakhir</b> : <strong>".$cv['dataPribadi']['edukasi']."</strong></h4>";
$html .= "<h4><b>Alamat</b></h4>";
$html .="<ul><b>- Jalan</b> : <strong>".$cv['dataPribadi']['address1']."</strong></ul>";
$html .="<ul><b>- Kota</b> : <strong>".$cv['dataPribadi']['kota']."</strong></ul>";
$html .="<ul><b>- Telepon</b> : <strong>".$cv['dataPribadi']['nomor']."</strong></ul>";
//PENDIDIKAN FORMAL RENDERING
$html .= '
    <h2>Pendidikan Formal</h2>
        <table border="1" cellpadding="4">
          <thead>
          <tr>
            <th><strong>Tingkat</strong></th>
            <th><strong>Lembaga Pendidikan</strong></th>
            <th><strong>Jurusan</strong></th>
            <th><strong>Tahun Lulus</strong></th>
          </tr>
          </thead>
          <tbody>';
foreach($cv['edukasi'] as $isinya){
              $html .= "<tr>";
              $html .= "<td>".$isinya['tingkat']."</td>";
              $html .= "<td>".$isinya['institusi']."</td>";
              $html .= "<td>".$isinya['gelar']."</td>";
              $html .= "<td>".$isinya['tahun']."</td>";
              $html .= "</tr>";
            }
$html .= '</tbody>
        </table>';

//PENDIDIKAN NON-FORMAL RENDERING
$html .= '
    <h2>Pendidikan Non Formal</h2>
        <table border="1" cellpadding="4">
          <thead>
          <tr>
            <th><strong>Nama Kursus</strong></th>
            <th><strong>Penyelenggara</strong></th>
            <th><strong>Mulai</strong></th>
            <th><strong>Selesai</strong></th>
          </tr>
          </thead>
          <tbody>';
foreach($cv['training'] as $isinya){
              $html .= "<tr>";
              $html .= "<td>".$isinya['judul']."</td>";
              $html .= "<td>".$isinya['institusi']."</td>";
              $html .= "<td>".$isinya['mulai']."</td>";
              $html .= "<td>".$isinya['selesai']."</td>";
              $html .= "</tr>";
            }

$html .= '</tbody>
        </table>';

//RIWAYAT JABATAN RENDERING
$html .= '
    <h2>Riwayat Jabatan</h2>
        <table border="1" cellpadding="4">
          <thead>
          <tr>
            <th><strong>Mulai</strong></th>
            <th><strong>Selesai</strong></th>
            <th><strong>Jabatan</strong></th>
          </tr>
          </thead>
          <tbody>';
foreach($cv['jabatan'] as $isinya){
              $html .= "<tr>";
              $html .= "<td>".$isinya['mulai']."</td>";
              $html .= "<td>".$isinya['selesai']."</td>";
              $html .= "<td>".$isinya['judul']."</td>";
              $html .= "</tr>";
            }

$html .= '</tbody>
        </table>';
if($special == 1){
//PENUGASAN KHUSUS RENDERING
$html .= '
    <h2>Penugasan Khusus</h2>
        <table border="1" cellpadding="4">
          <thead>
          <tr>
            <th><strong>Mulai</strong></th>
            <th><strong>Selesai</strong></th>
            <th><strong>Jabatan</strong></th>
          </tr>
          </thead>
          <tbody>';

foreach($cv['penugasan'] as $isinya){
              $html .= "<tr>";
              $html .= "<td>".$isinya['mulai']."</td>";
              $html .= "<td>".$isinya['selesai']."</td>";
              $html .= "<td>".$isinya['judul']."</td>";
              $html .= "</tr>";
            }

$html .= '</tbody>
        </table>';
}
//DATA KELUARGA RENDERING
$html .= '
    <h2>Data Keluarga</h2>
        <table border="1" cellpadding="4">
          <thead>
          <tr>
            <th><strong>Hubungan</strong></th>
            <th><strong>Nama</strong></th>
            <th><strong>Tempat / Tanggal Lahir</strong></th>
          </tr>
          </thead>
          <tbody>';
foreach($cv['keluarga'] as $isinya){
              $html .= "<tr>";
              $html .= "<td>".$isinya['hubungan']."</td>";
              $html .= "<td>".$isinya['nama']."</td>";
              $html .= "<td>".$isinya['ttl']."</td>";
              $html .= "</tr>";
            }
$html .= '</tbody>
        </table>';

//LISENSI RENDERING
$html .= '
    <h2>Lisensi</h2>
        <table border="1" cellpadding="4">
          <thead>
          <tr>
            <th><strong>Tipe Lisensi</strong></th>
            <th><strong>Tanggal Pembuatan</strong></th>
            <th><strong>Nomor Seri</strong></th>
          </tr>
          </thead>
          <tbody>';
foreach($cv['lisensi'] as $isinya){
              $html .= "<tr>";
              $html .= "<td>".$isinya['judul']."</td>";
              $html .= "<td>".$isinya['tgl']."</td>";
              $html .= "<td>".$isinya['nomorSeri']."</td>";
              $html .= "</tr>";
            }
$html .= '</tbody>
        </table>';

if($award == 1){
//PENGHARGAAN RENDERING
$html .= '
    <h2>Penghargaan</h2>
        <table border="1" cellpadding="4">
          <thead>
          <tr>
            <th><strong>Tanggal</strong></th>
            <th><strong>Jenis Penghargaan</strong></th>
          </tr>
          </thead>
          <tbody>';
foreach($cv['award'] as $isinya){
              $html .= "<tr>";
              $html .= "<td>".$isinya['tgl']."</td>";
              $html .= "<td>".$isinya['judul']."</td>";
              $html .= "</tr>";
            }
$html .= '</tbody>
        </table>';

}
if($punishment == 1){
//PELANGGARAN RENDERING
$html .= '
    <h2>Pelanggaran / Hukuman</h2>
        <table border="1" cellpadding="4">
          <thead>
          <tr>
            <th><strong>Tanggal</strong></th>
            <th><strong>Jenis Pelanggaran</strong></th>
            <th><strong>Jenis Hukuman</strong></th>
          </tr>
          </thead>
          <tbody>';
foreach($cv['award'] as $isinya){
              $html .= "<tr>";
              $html .= "<td>".$isinya['tgl']."</td>";
              $html .= "<td>".$isinya['judul']."</td>";
              $html .= "<td>".$isinya['deksripsi']."</td>";
              $html .= "</tr>";
            }
$html .= '</tbody>
        </table>';
}
$obj_pdf->writeHTML($html, true, false, true, false, '');
$obj_pdf->Output('output.pdf', 'I');
?>