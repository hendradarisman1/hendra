<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>Gapura Angkasa - Dashboard</title>

  <!-- Vendor stylesheet files. REQUIRED -->
  <!-- BEGIN: Vendor  -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/vendor.css">
  <!-- END: core stylesheet files -->

  <!-- Plugin stylesheet files. OPTIONAL -->

  <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/jqvmap/jqvmap.css">

  <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/dragula/dragula.css">

  <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/perfect-scrollbar/perfect-scrollbar.css">

  <!-- END: plugin stylesheet files -->

  <!-- Theme main stlesheet files. REQUIRED -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/chl.css">
  <link id="theme-list" rel="stylesheet" href="<?php echo base_url();?>assets/css/theme-peter-river.css">
  <!-- END: theme main stylesheet files -->

  <!-- begin pace.js  -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/pace/themes/blue/pace-theme-minimal.css">
  <script src="<?php echo base_url();?>assets/vendor/pace/pace.js"></script>
  <!-- END: pace.js  -->

</head>
<body>
  <!-- begin .app -->
  <div class="app">
    <!-- begin .app-wrap -->
    <div class="app-wrap">
      

      <!-- begin .app-container -->
      <div class="app-container">
        <!-- begin .app-main -->
        <div class="app-main">

          <!-- begin .main-heading -->
          
          <!-- END: .main-heading -->
<!-- begin .main-content -->
          <div class="main-content bg-clouds">

            <!-- begin .container-fluid -->
            <div class="container-fluid p-t-15">
              <!-- BEGIN: .row -->
              <br><br><br><br><br><br><br>
              <div class="row">
                <div class="center-block col-sm-6 col-sm-push-3">
                  <div class="f-1 text-orange text-center">
                    <div class="u-flex u-flexRow u-flexAlignItemsCenter">
                      <div class="f-800">4</div>
                      <div class="f-600">0</div>
                      <div class="f-300">4</div>
                      <span class="h3 text-muted p-l-25">
                        <i class="fa fa-fw fa-warning text-orange"></i>Page Not Found
                      </span>
                    </div>
                  </div>
                  <div class="d-b">
                    <p>
                      We could not find the page you were looking for. Meanwhile, you may return to
                      <a href="<?php echo base_url();?>index.php/dashboard"><Strong>dashboard</Strong></a> or contact your <b class="text-alizarin">administrative</b>.
                    </p>
                    <!-- <form action="index.html" method="post">
                      <div class="input-group">
                        <input name="search" class="form-control" placeholder="Search" type="text">

                        <div class="input-group-btn">
                          <button type="submit" name="submit" class="btn btn-warning btn-rect">
                            <i class="fa fa-search"></i>
                          </button>
                        </div>
                      </div>
                    </form> -->
                  </div>
                </div>
              </div>
              <!-- END: .row -->

            </div>
            <!-- END: .container-fluid -->

          </div>
          <!-- END: .main-content -->
        </div>
        <!-- END: .app-main -->
      </div>
      <!-- END: .app-container -->



      <!-- begin .app-footer -->
      <footer class="app-footer p-t-10 text-white">
        <div class="container-fluid">
          <p class="text-center small">
            Copyright Gapura Angkasa &copy; 2017
          </p>
        </div>
      </footer>
      <!-- END: .app-footer -->

    </div>
    <!-- END: .app-wrap -->
  </div>
  <!-- END: .app -->

  <span class="fa fa-angle-up" id="totop" data-plugin="totop"></span>

  <!-- Vendor javascript files. REQUIRED -->
  <script src="<?php echo base_url();?>assets/js/vendor.js"></script>
  <!-- END: End javascript files -->

  <!-- Plugin javascript files. OPTIONAL -->

  <script src="<?php echo base_url();?>assets/vendor/waypoints/jquery.waypoints.js"></script>
  <script src="<?php echo base_url();?>assets/vendor/counterup/jquery.counterup.js"></script>
  <script src="<?php echo base_url();?>assets/vendor/jqvmap/jquery.vmap.js"></script>
  <script src="<?php echo base_url();?>assets/vendor/jqvmap/jquery.vmap.sampledata.js"></script>
  <script src="<?php echo base_url();?>assets/vendor/jqvmap/maps/jquery.vmap.usa.js"></script>

  <script src="<?php echo base_url();?>assets/vendor/perfect-scrollbar/perfect-scrollbar.jquery.js"></script>

  <script src="<?php echo base_url();?>assets/vendor/dragula/dragula.js"></script>

  <script src="<?php echo base_url();?>assets/vendor/chart.js/Chart.js"></script>

  <script src="<?php echo base_url();?>assets/vendor/tablesorter/js/jquery.tablesorter.js"></script>

  <!-- END: plugin javascript files -->

  <!-- Demo javascript files. NOT REQUIRED -->

  <!-- END: demo javascript files -->

  <script src="<?php echo base_url();?>assets/js/chl.js"></script>
  <script src="<?php echo base_url();?>assets/js/display.js"></script>
  <script src="<?php echo base_url();?>assets/js/chl-demo.js"></script>

</body>

</html>
