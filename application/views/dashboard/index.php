<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>Gapura Angkasa - Dashboard</title>
  <link rel="icon" type="image/png" href="<?php echo base_url();?>/assets/img/gapuraicon.ico">
  <!-- Vendor stylesheet files. REQUIRED -->
  <!-- BEGIN: Vendor  -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/vendor.css">
  <!-- END: core stylesheet files -->

  <!-- Plugin stylesheet files. OPTIONAL -->

  <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/jqvmap/jqvmap.css">

  <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/dragula/dragula.css">

  <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/perfect-scrollbar/perfect-scrollbar.css">

  <!-- END: plugin stylesheet files -->

  <!-- Theme main stlesheet files. REQUIRED -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/chl.css">
  <link id="theme-list" rel="stylesheet" href="<?php echo base_url();?>assets/css/theme-peter-river.css">
  <!-- END: theme main stylesheet files -->

  <!-- begin pace.js  -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/pace/themes/blue/pace-theme-minimal.css">
  <script src="<?php echo base_url();?>assets/vendor/pace/pace.js"></script>
  <!-- END: pace.js  -->

</head>
<body>
  <!-- begin .app -->
  <div class="app">
    <!-- begin .app-wrap -->
    <div class="app-wrap">
      <!-- begin .app-heading -->
      <header class="app-heading">
        <header class="canvas is-fixed is-top bg-white p-v-15 shadow-4dp" id="top-search">

          <div class="container-fluid">
            <div class="input-group input-group-lg icon-before-input">
              <input type="text" class="form-control input-lg b-0" placeholder="Search for...">
              <div class="icon z-3">
                <i class="fa fa-fw fa-lg fa-search"></i>
              </div>
              <span class="input-group-btn">
                <button data-target="#top-search" data-toggle="canvas" class="btn btn-danger btn-line b-0">
                  <i class="fa fa-fw fa-lg fa-times"></i>
                </button>
              </span>
            </div>
            <!-- /input-group -->
          </div>

        </header>
        <!-- begin .navbar -->
        <nav class="navbar navbar-default navbar-static-top shadow-2dp">
          <!-- begin .navbar-header -->
          <div class="navbar-header">
            <!-- begin .navbar-header-left with image -->
            <div class="navbar-header-left b-r">
              <!--begin logo-->
              <a class="logo" href="<?php echo base_url();?>index.php/dashboard">
                <span class="logo-xs visible-xs">
                  <img src="<?php echo base_url();?>assets/img/logo_gapura.png" alt="logo-xs">
                </span>
                <span class="logo-lg hidden-xs">
                  <img src="<?php echo base_url();?>assets/img/logo_gapura.png" alt="logo-lg">
                </span>
              </a>
              <!--end logo-->
            </div>
            <!-- END: .navbar-header-left with image -->
            <nav class="nav navbar-header-nav">

              <a class="visible-xs b-r" href="#" data-side=collapse>
                <i class="fa fa-fw fa-bars"></i>
              </a>

              <a class="hidden-xs b-r" href="#" data-side=mini>
                <i class="fa fa-fw fa-bars"></i>
              </a>

            </nav>

            <ul class="nav navbar-header-nav m-l-a">
              <div class="bg-white text-black" style="padding-right: 10px;">
                    <div class="">
                      <div class="text-center h6">
                        <span data-momentum="clock" data-locale="fr" data-format="MMMM Do YYYY"></span>
                        <br>
                        <span class="f-6" data-momentum="clock"></span>
                      </div>
                    </div>
                  </div>
              </li>

              <li class="dropdown b-l">
                <a class="dropdown-toggle profile-pic" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                  <?php echo "<img class='img-circle' src='".$foto."'>"; ?>
                  <b class="hidden-xs hidden-sm"><?php echo $this->session->userdata('name');?></b>
                </a>
                <ul class="dropdown-menu animated flipInY pull-right">
                  <li>
					<li>
						<?php echo "<a href=''>ID : ".$this->session->userdata('uName')."</a>"; ?>
					</li>
					<li>
						<?php echo "<a href=''>Position : ".$this->session->userdata('position')."</a>"; ?>
					</li>
					<li>
						<a href=''>Authorization : <?php if($otorisasi[ADM]==1){echo"ADM";}if($otorisasi[HCM]==1){echo" HCM";}if($otorisasi[ADM] != 1 && $otorisasi[HCM] != 1){echo"PEG";}?></a>
					</li>
					<li role="separator" class="divider"></li>
				  </li>
				  <li>
                    <a href="<?php echo base_url()?>index.php/dashboard/logout">
                      <i class="fa fa-fw fa-sign-out"></i>
                      Logout
                    </a>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
          <!-- END: .navbar-header -->
        </nav>
        <!-- END: .navbar -->
      </header>
      <!-- END:  .app-heading -->

      <!-- begin .app-container -->
      <div class="app-container">

        <!-- begin .app-side -->
        <aside class="app-side">
          <!-- begin .side-content -->
          <div class="side-content">
            
            <!-- begin .side-nav -->
            <nav class="side-nav">
              <!-- BEGIN: nav-content -->
              <ul class="metismenu nav nav-inverse nav-bordered nav-stacked" data-plugin="metismenu">
                <br>
                <li class="nav-header">MAIN</li>

                <li>
                  <a class="active" href="<?php echo base_url();?>index.php/dashboard">
                    <span class="nav-icon">
                      <i class="fa fa-fw fa-cogs"></i>
                    </span>
                    <span class="nav-title">Dashboard</span>
                  </a>
                </li>

                <li class="nav-divider"></li>

                <li class="nav-header">MENU</li>

                <!-- BEGIN: Home -->
                <li id="home">
                  <a href="javascript:;">

                    <span class="nav-icon">
                      <i class="glyphicon glyphicon-home text-white"></i>
                    </span>
                    <span class="nav-title">Home</span>
                    <span class="nav-tools">
                      <i class="fa fa-fw arrow"></i>
                    </span>
                  </a>
                  <ul class="nav nav-sub nav-stacked">
                    <li id="news">
                      <a href="<?php echo base_url();?>index.php/home/news">News</a>
                    </li>
                    <li id="profil">
                      <a href="<?php echo base_url();?>index.php/home/profile">Profile</a>
                    </li>
                  </ul>
                </li>
                <!-- END: Home -->

                <!-- BEGIN: Human Capital -->
                <li id="hc">
                  <a href="javascript:;">
                    <span class="nav-icon">
                      <i class="glyphicon glyphicon-book text-peter-river"></i>
                    </span>
                    <span class="nav-title">Human Capital</span>
                    <span class="nav-tools">
                      <i class="fa fa-fw arrow"></i>
                    </span>
                  </a>
                  <ul class="nav nav-sub nav-stacked">
                    <li>
                      <a href="" data-toggle="modal" data-target=".cv-user-modal" data-toggle='modal' target="blank">Curriculum Vitae (CV)</a>
                    </li>
                    <li>
                      <a href="<?php echo base_url();?>index.php/humanCapital/payslip">Payslip</a>
                    </li>

                    <?php
                      if($otorisasi['HCM'] == 1){
                        echo '
                      <li id="upload">
                      <a href="javascript:;">
                        <span class="nav-title">Upload</span>
                        <span class="nav-tools">
                          <i class="fa fa-fw arrow"></i>
                        </span>
                      </a>
                      <ul class="nav nav-sub" aria-expanded="false">
                        <li>
                          <a href="'.base_url().'index.php/humanCapital/deductions">
                            <span class="nav-title">Deduction</span>
                          </a>
                        </li>
                        <li>
                          <a href="'.base_url().'index.php/humanCapital/additionalPayments">
                            <span class="nav-title">Additional Payments</span>
                          </a>
                        </li>
                        <li>
                          <a href="'.base_url().'index.php/humanCapital/familyMembers">
                            <span class="nav-title">Family Member/Dependents</span>
                          </a>
                        </li>
                        <li>
                          <a href="'.base_url().'index.php/humanCapital/education">
                            <span class="nav-title">Education</span>
                          </a>
                        </li>
                      </ul>
                    </li>';
                      }
                    ?>
                  </ul>
                </li>
                <!-- END: Human Capital -->

                <!-- BEGIN: Administrative -->
                <?php
                  if($otorisasi['ADM'] == 1){
                    echo '
                    <li id="admin">
                      <a href="'.base_url().'index.php/administrative">
                        <span class="nav-icon">
                          <i class="fa fa-fw fa fa-superpowers text-alizarin"></i>
                        </span>
                        <span class="nav-title">Administrative</span>
                      </a>
                    </li>';
                  }
                ?>
                <!-- END: Administrative -->

                <li class="nav-divider"></li>

                <!-- BEGIN: utility -->
                <li>
                  <a href="javascript:;">
                    <span class="nav-icon">
                      <i class="fa fa-external-link"></i>
                    </span>
                    <span class="nav-title">Links</span>
                    <span class="nav-tools">
                      <i class="fa fa-fw arrow"></i>
                    </span>
                  </a>
                  <ul class="nav nav-sub nav-stacked">
                    <li>
                      <a class="text-peter-river" href="http://www.gapura.id/">Gapura.id</a>
                    </li>
                    <li>
                      <a class="text-peter-river" href="https://www.garuda-indonesia.com">Garuda-indonesia.com</a>
                    </li>
                  </ul>
                </li>
                <!-- BEGIN: utility -->
              </ul>
              <!-- END: nav-content -->
            </nav>
            <!-- END: .side-nav -->
          </div>
          <!-- END: .side-content -->
        </aside>
        <!-- END: .app-side -->

        <!-- begin side-collapse-visible bar -->
        <div class="side-visible-line hidden-xs" data-side="collapse">
          <i class="fa fa-caret-left"></i>
        </div>
        <!-- begin side-collapse-visible bar -->

        <!-- begin .app-main -->
        <div class="app-main">

          <!-- begin .main-heading -->
          <header class="main-heading shadow-2dp">
            <!-- begin dashhead -->
            <div class="dashhead bg-white">
              <div class="dashhead-titles">
                <h6 class="dashhead-subtitle">
                  Gapura Angkasa
                </h6>
                <h3 class="dashhead-title">Dashboard</h3>
              </div>

              <div class="dashhead-toolbar">
                <div class="dashhead-toolbar-item">
                  <a href="http://www.gapura.id/" target="frame">Gapura Angkasa</a>
                  / Dashboard
                </div>
              </div>
            </div>
            <!-- END: dashhead -->
          </header>
          <!-- END: .main-heading -->
            <!-- begin MAIN CONTENT -->    
            <!-- <p>ayam</p> -->
            <div class="embed-responsive embed-responsive-4by3">
              <iframe name="frame" class="embed-responsive-item" src="http://www.gapura.id/"></iframe>
            </div>
            <!-- END: MAIN CONTENT -->
        </div>
        <!-- END: .app-main -->
      </div>
      <!-- END: .app-container -->
      <!--BEGIN: .modal cv user-->
      <div id="dataModal" class="modal fade cv-user-modal">
        <div class="modal-dialog modal-sm">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h5 class="text-alizarin text-uppercase"><Strong>Generate your cv now?</Strong></h5> 
            </div>
            <form class="form-horizontal" action="<?php echo base_url();?>index.php/humanCapital/cvpdf" method="post" target="_blank">
            <div class="modal-body">
              <div class="text-center">
                <h4>Additional Option</h4><br> 
              </div>
             <div class="form-group">
              <div class="col-sm-10">
                <div class="checkbox">
                  <label>
                    <input value="1" checked="" name="award" type="checkbox"> Award Data
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input value="1" checked="" name="punishment" type="checkbox"> Punishment Data
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input value="1" checked="" name="special" type="checkbox"> Special Assignment
                  </label>
                </div>
              </div>
             </div>
            </div>
            <div class="modal-footer">
              <div class="col-sm-6 col-md-12">
                  <button type="submit" class="btn btn-flat btn-success pull-left"><span class="fa fa-check-circle"></span> Procced</button>
                  <button class="btn btn-flat btn-danger pull-right" data-dismiss="modal"><span class="fa fa-close"></span> Dismiss</button>
              </div>       
            </div>
            </form>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
      <!--END: .modal cv user -->
      <!-- begin .app-footer -->
      <footer class="app-footer p-t-10 text-white">
        <div class="container-fluid">
          <p class="text-center small">
            Copyright Gapura Angkasa &copy; 2017
          </p>
        </div>
      </footer>
      <!-- END: .app-footer -->

    </div>
    <!-- END: .app-wrap -->
  </div>
  <!-- END: .app -->

  <span class="fa fa-angle-up" id="totop" data-plugin="totop"></span>

  <!-- Vendor javascript files. REQUIRED -->
  <script src="<?php echo base_url();?>assets/js/vendor.js"></script>
  <!-- END: End javascript files -->

  <!-- Plugin javascript files. OPTIONAL -->

  <script src="<?php echo base_url();?>assets/vendor/waypoints/jquery.waypoints.js"></script>
  <script src="<?php echo base_url();?>assets/vendor/counterup/jquery.counterup.js"></script>
  <script src="<?php echo base_url();?>assets/vendor/jqvmap/jquery.vmap.js"></script>
  <script src="<?php echo base_url();?>assets/vendor/jqvmap/jquery.vmap.sampledata.js"></script>
  <script src="<?php echo base_url();?>assets/vendor/jqvmap/maps/jquery.vmap.usa.js"></script>

  <script src="<?php echo base_url();?>assets/vendor/perfect-scrollbar/perfect-scrollbar.jquery.js"></script>

  <script src="<?php echo base_url();?>assets/vendor/dragula/dragula.js"></script>

  <script src="<?php echo base_url();?>assets/vendor/chart.js/Chart.js"></script>

  <script src="<?php echo base_url();?>assets/vendor/tablesorter/js/jquery.tablesorter.js"></script>

  <!-- END: plugin javascript files -->

  <!-- Demo javascript files. NOT REQUIRED -->

  <!-- END: demo javascript files -->

  <script src="<?php echo base_url();?>assets/js/chl.js"></script>
  <script src="<?php echo base_url();?>assets/js/display.js"></script>
  <script src="<?php echo base_url();?>assets/js/chl-demo.js"></script>

</body>

</html>
