<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniters</title>

	<style type="text/css">

	::selection{ background-color: #E13300; color: white; }
	::moz-selection{ background-color: #E13300; color: white; }
	::webkit-selection{ background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body{
		margin: 0 15px 0 15px;
	}
	
	p.footer{
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}
	
	#container{
		margin: 10px;
		border: 1px solid #D0D0D0;
		-webkit-box-shadow: 0 0 8px #D0D0D0;
	}
	</style>
</head>
<body>

<div id="container">
	<h1>Welcome to CodeIgniter!</h1>

	<div id="body">
		<a href="<?php echo base_url();?>index.php/gapura/profile/">profil</a>
			<form method="POST" action="<?php echo base_url()?>index.php/gapura/otorisasi">
				<label for="nopeg"></label>
				<input type="text" name="nopeg">				
				<input type="submit" name="ok">
			</form>
			<!--<h1>Username</h1>
			<?php
				echo "<h2>".$cv['dataPribadi']['nama']."</h2>";
				echo "<br>";
				echo "<h3><strong>Nopeg: </strong></h3>";
				echo "<h3>".$cv['dataPribadi']['nopeg']."</h3>";
				echo "<br>";
				echo "<h3><strong>Jabatan: </strong></h3>";
				echo "<h3>".$cv['dataPribadi']['jabatan']."</h3>";
				echo "<br>";
				echo "<h3><strong>Unit Kerja: </strong></h3>";
				echo "<h3>".$cv['dataPribadi']['divisi']."</h3>";
				echo "<br>";
				echo "<h3><strong>Lokasi: </strong></h3>";
				echo "<h3>".$cv['dataPribadi']['lokasi']."</h3>";
				echo "<br>";
				?>
				<h2>Profil</h2>
				<hr>
				<?php
					echo "<h3>Tempat/Tgl Lahir: </h3>";
					echo "<h3>".$cv['dataPribadi']['ttl']."</h3>";
					echo "<br>";
					echo "<h3>Jenis Kelamin: </h3>";
					echo "<h3>".$cv['dataPribadi']['gender']."</h3>";
					echo "<br>";
					echo "<h3>Agama: </h3>";
					echo "<h3>".$cv['dataPribadi']['agama']."</h3>";
					echo "<br>";
					echo "<h3>Tgl Mulai Bekerja: </h3>";
					echo "<h3>".$cv['dataPribadi']['hireDate']."</h3>";
					echo "<br>";
					echo "<h3>Status Karyawan: </h3>";
					echo "<h3>".$cv['dataPribadi']['statusPeg']."</h3>";
					echo "<br>";
					echo "<h3>Status Keluarga: </h3>";
					echo "<h3>".$cv['dataPribadi']['statusNikah']."</h3>";
					echo "<br>";
					echo "<h3>Pendidikan Terakhir</h3>";
					echo "<h3>".$cv['dataPribadi']['edukasi']."</h3>";
					echo "<br>";
					echo "<h3>Alamat</h3>";
					echo "<br>";
					echo "<h3>- Jalan: </h3>";
					echo "<h3>".$cv['dataPribadi']['address1']."</h3>";
					echo "<br>";
					echo "<h3>- Kota: </h3>";
					echo "<h3>".$cv['dataPribadi']['kota']."</h3>";
					echo "<br>";
					echo "<h3>- Telepon: </h3>";
					echo "<h3>".$cv['dataPribadi']['nomor']."</h3>";
					echo "<br>";
				?>
				<h2>Pendidikan Formal</h2>
				<table>
					<thead>
					<tr>
						<th>Tingkat</th>
						<th>Lembaga Pendidikan</th>
						<th>Jurusan</th>
						<th>Tahun Lulus</th>
					</tr>
					</thead>
					<tbody>
						<?php
						// edukasinya
						foreach($cv['edukasi'] as $isinya){
							echo "<tr>";
							echo "<td>".$isinya['tingkat']."</td>";
							echo "<td>".$isinya['institusi']."</td>";
							echo "<td>".$isinya['gelar']."</td>";
							echo "<td>".$isinya['tahun']."</td>";
							echo"</tr>";
						}
						?>		
					</tbody>
				</table>
				
				<h2>Pendidikan Non Formal</h2>
				<table>
					<thead>
					<tr>
						<th>Nama Kursus</th>
						<th>Penyelenggara</th>
						<th>Mulai</th>
						<th>Selesai</th>
					</tr>
					</thead>
					<tbody>
						<?php
						// trainingnya
						foreach($cv['training'] as $isinya){
							echo"<tr>";
							echo "<td>".$isinya['judul']."</td>";
							echo "<td>".$isinya['institusi']."</td>";
							echo "<td>".$isinya['mulai']."</td>";
							echo "<td>".$isinya['selesai']."</td>";
							echo"</tr>";
						}
						?>
					</tbody>
				</table>

				<h2>Riwayat Jabatan</h2>
				<table>
					<thead>
					<tr>
						<th>Mulai</th>
						<th>Selesai</th>
						<th>Jabatan</th>
					</tr>
					</thead>
					<tbody>
						<?php
						// riwayat jabatannya
						foreach($cv['jabatan'] as $isinya){
							echo"<tr>";
							echo "<td>".$isinya['mulai']."</td>";
							echo "<td>".$isinya['selesai']."</td>";
							echo "<td>".$isinya['judul']."</td>";
							echo"</tr>";
						}
						?>
					</tbody>
				</table>

				<h2>Penugasan Khusus</h2>
				<table>
					<thead>
					<tr>
						<th>Mulai</th>
						<th>Selesai</th>
						<th>Jabatan</th>
					</tr>
					</thead>
					<tbody>
						<?php
						// riwayat jabatannya
						foreach($cv['penugasan'] as $isinya){
							echo"<tr>";
							echo "<td>".$isinya['mulai']."</td>";
							echo "<td>".$isinya['selesai']."</td>";
							echo "<td>".$isinya['judul']."</td>";
							echo"</tr>";
						}
						?>
					</tbody>
				</table>

				<h2>Data Keluarga</h2>
				<table>
					<thead>
					<tr>
						<th>Hubungan</th>
						<th>Nama</th>
						<th>Tempat / Tanggal Lahir</th>
					</tr>
					</thead>
					<tbody>
						<?php
						// riwayat jabatannya
						foreach($cv['keluarga'] as $isinya){
							echo"<tr>";
							echo "<td>".$isinya['hubungan']."</td>";
							echo "<td>".$isinya['nama']."</td>";
							echo "<td>".$isinya['ttl']."</td>";
							echo"</tr>";
						}
						?>
					</tbody>
				</table>

				<h2>Lisensi</h2>
				<table>
					<thead>
					<tr>
						<th>Tipe Lisensi</th>
						<th>Tanggal Pembuatan</th>
						<th>Nomor Seri</th>
					</tr>
					</thead>
					<tbody>
						<?php
						// riwayat jabatannya
						foreach($cv['lisensi'] as $isinya){
							echo"<tr>";
							echo "<td>".$isinya['judul']."</td>";
							echo "<td>".$isinya['tgl']."</td>";
							echo "<td>".$isinya['nomorSeri']."</td>";
							echo"</tr>";
						}
						?>
					</tbody>
				</table>

				<h2>Penghargaan</h2>
				<table>
					<thead>
					<tr>
						<th>Tanggal</th>
						<th>Jenis Penghargaan</th>
					</tr>
					</thead>
					<tbody>
						<?php
						// riwayat jabatannya
						foreach($cv['award'] as $isinya){
							echo"<tr>";
							echo "<td>".$isinya['tgl']."</td>";
							echo "<td>".$isinya['judul']."</td>";
							echo"</tr>";
						}
						?>
					</tbody>
				</table>

				<h2>Pelanggaran / Hukuman</h2>
				<table>
					<thead>
					<tr>
						<th>Tanggal</th>
						<th>Tingkat Pelanggaran</th>
						<th>Jenis Hukuman</th>
					</tr>
					</thead>
					<tbody>
						<?php
						// riwayat jabatannya
						foreach($cv['award'] as $isinya){
							echo"<tr>";
							echo "<td>".$isinya['tgl']."</td>";
							echo "<td>".$isinya['judul']."</td>";
							echo "<td>".$isinya['deksripsi']."</td>";
							echo"</tr>";
						}
						?>
					</tbody>
				</table>
			<br>-->
			<!-- <form action="<?php echo base_url();?>index.php/administrative/upload/" method="POST">
			<input type="file" name="userfile"></input>
			<br>
			<h1>Password</h1>
			<input type="password" name="password"></input>
			<br>
			<button type="submit">submit</button>
			</form> -->
			<?php #echo $error;?> <!-- Error Message will show up here -->
			<?php #echo form_open_multipart('administrative/signupMpyProc');?>
			<?php #echo "<input type='file' name='userfile' size='20' />"; ?>
			<?php #echo "<input type='submit' name='submit' value='upload' /> ";?>
			<?php #echo "</form>"?>
		<!-- <form method="post" action="welcome/signupProcess/">
			
		</form> -->
	<!--</div>-->

	<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds</p>
</div>

</body>
</html>