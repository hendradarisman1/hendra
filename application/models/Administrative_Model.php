<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Administrative_Model extends CI_Model {	


	public function getAllUser(){
        $this->db->select('key,username,otorisasi,fullname,status');
        $query = $this->db->get('user');
        return $query->result_array();
    }

    public function changeAuthorization($nopeg, $authorization){
    	$upt = "UPDATE user SET otorisasi = ? WHERE username = ?";
      $query = $this->db->query($upt,array($authorization, $nopeg));
      if ($query==0) {
        return "gagal";
      }
      else{
        return "jalan";
      }
    }

    public function deleteUser($nopeg){
    	$this->db->where('username',$nopeg);
		$this->db->delete('user');
    }

    public function lock($nopeg){
    	$upt = "UPDATE user SET status = ? WHERE username = ?";
		$query = $this->db->query($upt,array('NON AKTIF',$nopeg));
    }

    public function unLock($nopeg){
    	$upt = "UPDATE user SET status = ? WHERE username = ?";
		$query = $this->db->query($upt,array('AKTIF',$nopeg));
    }

    public function signup($data){
    	$rs = $this->db->insert('user',$data);
    }

    public function signupMpy($data){
        $this->db->insert('user',$data);
    }
}
?>