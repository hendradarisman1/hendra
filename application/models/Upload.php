<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Upload extends CI_Model {	

	public function infoType14($nopeg, $wageType, $openperiod, $closeperiod, $amount){
		// Membuat koneksi ke sap
		$sap = new SAPConnection();
		$sap->Connect();

		if($sap->getStatus() == SAPRFC_OK) $sap->open();
		if ($sap->GetStatus() != SAPRFC_OK ) {
		       #$sap->PrintStatus();
		       exit;
		    }
		    // Memanggil fungsi untuk jadi objek
		$fce = $sap->NewFunction ("ZHRPA_DEDUCTION");
		    if ($fce == false ) {
		      echo "gajalan";
		       // $sap->PrintStatus();
		       exit;
		    }
		    // Dalam currenc IDR, akan selalu excess dua digit nol, sehingga harus dibagi 100 terlebih dahulu
		$amount = (string)($amount/100);
		$nowDate = (string)date("Ymd");

		$fce->I_MODE='INS';
		$fce->I_TCLAS='A';

// Internal table pa0014
		$fce->I_DATA['MANDT']='010';
		$fce->I_DATA['PERNR']=$nopeg;
		$fce->I_DATA['SUBTY']=$wageType;
		$fce->I_DATA['ENDDA']='99991231';
		$fce->I_DATA['BEGDA']=$openperiod;//01.07.2017
		$fce->I_DATA['AEDTM']=$nowDate;
		$fce->I_DATA['UNAME']='ASYSTDK';
		$fce->I_DATA['LGART']=$wageType;
		$fce->I_DATA['BETRG']=$amount;
		$fce->I_DATA['WAERS']='IDR';
		$fce->I_DATA['ANZHL']='0,00';

		$fce->Call();
		if(isset($fce->ERR_MESSAGES)){
			
		}
		else{
		  //var_dump($fce->ERR_MESSAGES);
		  //$fce->PrintStatus();	
		}
	}

		public function infoType15($nopeg, $wageType, $openperiod, $closeperiod, $amount){
		// Membuat koneksi ke sap
		$sap = new SAPConnection();
		$sap->Connect();

		if($sap->getStatus() == SAPRFC_OK) $sap->open();
		if ($sap->GetStatus() != SAPRFC_OK ) {
		       #$sap->PrintStatus();
		       exit;
		    }
		    // Memanggil fungsi untuk jadi objek
		$fce = $sap->NewFunction ("ZHRPA_ADDITIONAL_PAYMENT");
		    if ($fce == false ) {
		      echo "gajalan";
		       // $sap->PrintStatus();
		       exit;
		    }
		    // Dalam currenc IDR, akan selalu excess dua digit nol, sehingga harus dibagi 100 terlebih dahulu
		$amount = (string)($amount/100);
		$nowDate = (string)date("Ymd");

		$fce->I_MODE='INS';
		$fce->I_TCLAS='A';

// Internal table pa0015
		$fce->I_DATA['MANDT']='010';
		$fce->I_DATA['PERNR']=$nopeg;
		$fce->I_DATA['SUBTY']=$wageType;
		$fce->I_DATA['ENDDA']=$closeperiod;
		$fce->I_DATA['BEGDA']=$openperiod;//01.07.2017
		$fce->I_DATA['AEDTM']=$nowDate;
		$fce->I_DATA['UNAME']='ASYSTDK';
		$fce->I_DATA['LGART']=$wageType;
		$fce->I_DATA['BETRG']=$amount;
		$fce->I_DATA['WAERS']='IDR';
		$fce->I_DATA['ANZHL']='0,00';

		$fce->Call();
		if(isset($fce->ERR_MESSAGES)){
			
		}
		else{
		  //var_dump($fce->ERR_MESSAGES);
			//$fce->PrintStatus();	
		}
	}

	// Gak dipake

	/*public function multipleInfoType15($filePath){
		// Membuat koneksi ke sap
		$sap = new SAPConnection();
		$sap->Connect();

		if($sap->getStatus() == SAPRFC_OK) $sap->open();
		if ($sap->GetStatus() != SAPRFC_OK ) {
		       #$sap->PrintStatus();
		       exit;
		    }
		    // Memanggil fungsi untuk jadi objek
		$fce = $sap->NewFunction ("ZHRPA_UPLOAD_0015");
		    if ($fce == false ) {
		      echo "gajalan";
		       // $sap->PrintStatus();
		       exit;
		    }
		$fce->FILENAME=$filePath;
		$fce->FILETYPE='DAT';
		$fce->CHECKPOI=' ';
		$fce->TRANSMOD='N';

		$fce->Call();

		echo($fce->FILENAME);
		$fce->PrintStatus();
	}*/

	public function infoType21($nopeg, $familyType, $birth, $birthplace, $gender, $name, $konsesi){
		// Membuat koneksi ke sap
		$sap = new SAPConnection();
		$sap->Connect();

		if($sap->getStatus() == SAPRFC_OK) $sap->open();
		if ($sap->GetStatus() != SAPRFC_OK ) {
		       #$sap->PrintStatus();
		       exit;
		    }
		    // Memanggil fungsi untuk jadi objek
		$fce = $sap->NewFunction ("ZHRPA_FAMILY_ID");
		    if ($fce == false ) {
		      echo "gajalan";
		       // $sap->PrintStatus();
		       exit;
		    };
		//Ambil tanggal hari ini
		$nowDate = (string)date("Ymd");

		$fce->I_MODE='INS';
		$fce->I_TCLAS='A';
		// Data PA0021
		$fce->I_DATA['MANDT']='010';
		$fce->I_DATA['PERNR']=$nopeg;
		$fce->I_DATA['SUBTY']=$familyType;
		$fce->I_DATA['ENDDA']='99991231';
		$fce->I_DATA['BEGDA']=$nowDate;//01.07.2017
		$fce->I_DATA['AEDTM']=$nowDate;
		$fce->I_DATA['UNAME']='ASYSTDK';
		$fce->I_DATA['FAMSA']=$familyType;
		$fce->I_DATA['FGBDT']=$birth;
		$fce->I_DATA['FGBOT']=$birthplace;
		$fce->I_DATA['FANAT']='ID';
		$fce->I_DATA['FGBLD']='ID';
		$fce->I_DATA['FASEX']=$gender;
		/*
		1:male
		2:female
		*/
		$fce->I_DATA['FCNAM']=$name;
		$fce->I_DATA['KDZUG']=$konsesi;

		$fce->I_BEGDA=$nowDate;
		$fce->I_ENDDA='99991231';
		// Data PA0318
		$fce->I_SECONDDATA['MANDT']='010';
		$fce->I_SECONDDATA['PERNR']=$nopeg;
		$fce->I_SECONDDATA['SUBTY']=$familyType;
		$fce->I_SECONDDATA['ENDDA']='99991231';
		$fce->I_SECONDDATA['BEGDA']=$nowDate;//01.07.2017
		$fce->I_SECONDDATA['AEDTM']=$nowDate;
		$fce->I_SECONDDATA['UNAME']='ASYSTDK';
		$fce->I_SECONDDATA['ZZFAMSTAT']='01';
		// $fce->I_SECONDDATA['ZZCHILD']='01';

		$fce->Call();
		if(isset($fce->ERR_MESSAGES)){
			
		}
		else{
		  //var_dump($fce->ERR_MESSAGES);
			//$fce->PrintStatus();	
		}
	}

	public function infoType22($nopeg,$educationType,$start,$end,$lokasi,$country,$courseName){
		// Membuat koneksi ke sap
		$sap = new SAPConnection();
		$sap->Connect();

		if($sap->getStatus() == SAPRFC_OK) $sap->open();
		if ($sap->GetStatus() != SAPRFC_OK ) {
		       #$sap->PrintStatus();
		       exit;
		    }
		    // Memanggil fungsi untuk jadi objek
		$fce = $sap->NewFunction ("ZHRPA_EDUCATION");
		    if ($fce == false ) {
		      echo "gajalan";
		       // $sap->PrintStatus();
		       exit;
		    };
		//Ambil tanggal hari ini
		$nowDate = (string)date("Ymd");

		$fce->I_MODE='INS';
		$fce->I_TCLAS='A';
		// Data PA0022
		$fce->I_DATA['MANDT']='010';
		$fce->I_DATA['PERNR']=$nopeg;
		$fce->I_DATA['SUBTY']=$educationType;
		$fce->I_DATA['ENDDA']=$end;
		$fce->I_DATA['BEGDA']=$start;//01.07.2017
		$fce->I_DATA['AEDTM']=$nowDate;
		$fce->I_DATA['UNAME']='ASYSTDK';
		$fce->I_DATA['SLART']=$educationType;
		$fce->I_DATA['INSTI']=$lokasi;
		$fce->I_DATA['SLAND']=$country;
		$fce->I_DATA['COURSE_NM']=$courseName;
		$fce->I_DATA['SLABS']='10';
	
		$fce->Call();
		if(isset($fce->ERR_MESSAGES)){
			
		}
		else{
		  //var_dump($fce->ERR_MESSAGES);
			//$fce->PrintStatus();	
		}
	}
}
?>