<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Upload_model extends CI_Model {	

	public function login($username, $password){
		$sql = "SELECT username, password, salt, otorisasi, status FROM user WHERE username = ?"; 

		$hasil = $this->db->query($sql, $username);
		// var_dump($hasil);
		$row = $hasil->row();
		$passNsalt = md5($password.$row->salt);
		if($row->password == $passNsalt){
			if ($row->status != 'AKTIF') {
				$userSession = 'frozed';
				return $userSession;
			}
			if($username=="adminsm"||$username=="adminhc"){
				$userSession = array(
					'uName' => $username,
					'name' => $username,
				);
			}
			else{
				$userSession = $this->getPersData($username);
			}
			// var_dump($userInfo);
			// echo "login berhasil session dibuat";
			// $userSession = array(
			// 		'uName' => $row->username,
			// 		'namaLengkap' => $row->namalengkap
			// 	);
			// $this->session->set_userdata(
			// 		array(
			// 			'uName' => $row->username,
			// 			'namaLengkap' => $row->namalengkap
			// 		)
			// );
		}
		else{
			$userSession = array(
					'uName' => NULL
				);
		}
		return $userSession;
	}

	public function signUp(){

	}

	public function getPhoto($nopeg){
		$nowDate = (string)date("Ymd");
		$sap = new SAPConnection();
		$sap->Connect();

		if($sap->getStatus() == SAPRFC_OK) $sap->open();
			if ($sap->GetStatus() != SAPRFC_OK ) {
       			$sap->PrintStatus();
       			exit;
    		}
	  $fce = $sap->NewFunction ("HRWPC_RFC_EP_READ_PHOTO_URI");
	  if ($fce == false ) {
	         // $sap->PrintStatus();
	         exit;
	      }

	  $fce->PERNR=$nopeg;
	  $fce->DATUM=$nowDate;
	  $fce->TCLAS="A";

	  $fce->Call();
	  if($fce->GetStatus()== SAPRFC_OK){
	     // var_dump($fce->URI);
	     return $fce->URI;
	    // echo $fce->URI;
	  }
	  else{
	  	return base_url().'assets/img/admin.png';
	     $fce->PrintStatus();
	  }
	}

	public function getPersData($nopeg){
		$sap = new SAPConnection();
		$sap->Connect();

		if($sap->getStatus() == SAPRFC_OK) $sap->open();
			if ($sap->GetStatus() != SAPRFC_OK ) {
       			$this->session->set_flashdata('response',"<div class='alert alert-danger'><b>Your Network Connection is prohibited</b></div>");
				redirect('/gapura/');
    		}

    	$fce = $sap->NewFunction ("ZHR_GET_EMPLOYEE_DATA");
		if ($fce == false ) {
		       // $sap->PrintStatus();
		       exit;
		    }
		// Inisialisasi Variabel
		$nowDate = (string)date("Ymd");


		$fce->PERSON_ID=$nopeg;
		$fce->SELECTION_BEGIN=$nowDate;
		$fce->SELECTION_END=$nowDate;

		$fce->Call();
		 if($fce->GetStatus()== SAPRFC_OK){
		 	$persData = array(
		 		'name' => $fce->PERSONAL_DATA["ENAME"],
		 		'uName' => sprintf("%07d",$fce->PERSONAL_DATA["PERNR"]),
		 		'position' => $fce->PERSONAL_DATA["PLANS_TXT"],
		 		'orgUnit' => $fce->PERSONAL_DATA["ORGEH_TXT"],
		 		'status' => $fce->PERSONAL_DATA["STATUS"],
		 		'birthDate' => $fce->PERSONAL_DATA["GBDAT"]
		 		);
		 	return $persData;
		 }
		 else{
		 	$fce->PrintStatus();
		 	$this->session->set_flashdata('response',"<div class='alert alert-warning'><b>Your Network Connection is prohibited</b></div>");
			redirect('/gapura/');
		 }
		}

	public function logout(){
		$this->session->sess_destroy();
	}

	public function cekOtorisasi($nopeg){
		$sql = "SELECT username, otorisasi FROM user WHERE username = ?"; 
		$hasil = $this->db->query($sql, $nopeg);
		$row = $hasil->row();
		$otorisasi = $row->otorisasi;
		return $otorisasi;
	}

	public function cekStatus($nopeg){
		$sql = "SELECT status FROM user WHERE username = ?"; 
		$hasil = $this->db->query($sql, $nopeg);
		$row = $hasil->row();
		$status = $row->status;
		return $status;
	}

	public function cekUser($nopeg){
		$sql = "SELECT username FROM user WHERE username = ?";
		$hasil = $this->db->query($sql, $nopeg);
		$row = $hasil->row();
		$useravailability = $row->username;
		return $useravailability;
	}

	public function gantiPassword($nopeg, $passwordlama, $passwordbaru){
		$sql = "SELECT username, password, salt FROM user WHERE username = ?"; 

		$hasil = $this->db->query($sql, $nopeg);
		// var_dump($hasil);
		$row = $hasil->row();
		$passNsalt = md5($passwordlama.$row->salt);
		if($row->password == $passNsalt){
			$salt = uniqid();
			$passNsalt= md5($passwordbaru.$salt);
			$query = "UPDATE user SET password = ?, salt = ? WHERE username = ?";
			$hasil = $this->db->query($query, array($passNsalt, $salt, $nopeg));
			return 1;
			}
		else{
			return 0;
			echo "Salah memasukkan password lama";
		}
	}

	public function cekNoPegExists($nopeg){
		$sap = new SAPConnection();
		$sap->Connect();

		if($sap->getStatus() == SAPRFC_OK) $sap->open();
		if ($sap->GetStatus() != SAPRFC_OK ) {
		       #$sap->PrintStatus();
		       exit;
		    }

		// Inisialisasi variabel
		$nowDate = (string)date("Ymd");

		// FM untuk check existing
		$fce = $sap->NewFunction ("BAPI_EMPLOYEE_CHECKEXISTENCE");
		    if ($fce == false ) {
		      echo "gajalan";
		       // $sap->PrintStatus();
		       exit;
		    }

		$fce->NUMBER = $nopeg;

		$fce->Call();

		if($fce->RETURN['MESSAGE']==''){
			return 1;
		}
		else{
			return 0;
		}
	}
}
?>