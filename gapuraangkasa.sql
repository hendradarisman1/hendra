-- phpMyAdmin SQL Dump
-- version 3.1.3.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 03, 2017 at 10:33 AM
-- Server version: 5.1.33
-- PHP Version: 5.2.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `gapuraangkasa`
--

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `key` int(11) NOT NULL AUTO_INCREMENT,
  `username` char(7) NOT NULL,
  `otorisasi` varchar(7) NOT NULL,
  `password` varchar(200) NOT NULL,
  `salt` varchar(200) NOT NULL,
  PRIMARY KEY (`key`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`key`, `username`, `otorisasi`, `password`, `salt`) VALUES
(4, 'admin', 'ADM', 'ac5df430ba0c2cf86d8572b2c8e397c2', '597e9695f32ac'),
(6, '0000002', 'PEG', '845df86a6a45666123d308675d0b6a26', '59813aadc1453'),
(7, '2000000', 'PEG', '2f240857eff756d4cd8f416f59028afe', '59814e2b13588'),
(16, '0000001', 'PEG', '6a2393d12ad6d5d0ba76f4249db40c28', '598185cc3262c'),
(17, '0000003', 'PEG', 'f2330eefd580d47e7e636470f72bfb8a', '598279671c6ab');
